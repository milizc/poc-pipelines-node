//  "type": "module",
// import express from 'express';
// import bodyParser from 'body-parser';
require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const errorHandler = require('./src/middlewares/error-handler');
const userController = require('./src/controllers/users.controller');


const app = express();
app.use(bodyParser.json());


app.get('/hello', function (req, res) {
    res.send('Hello World');
})

app.use('/users', userController);

// global error handler
app.use(errorHandler);
console.log(process.env.PORT);
app.listen(process.env.PORT);